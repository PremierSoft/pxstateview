//
//  DefaultActionStateRepresentable.swift
//  PxStateView
//
//  Created by Orlando Amorim on 12/06/19.
//  Copyright © 2019 Pixelwolf. All rights reserved.
//

import Foundation

public struct DefaultPxStateRepresentable: PxStateRepresentable {
    public var elements: [PxStateViewElement]
    public var configuration: PxStateViewConfiguration
    
    public init(elements: [PxStateViewElement], configuration: PxStateViewConfiguration = PxStateViewConfiguration()) {
        self.elements = elements
        self.configuration = configuration
    }
}
