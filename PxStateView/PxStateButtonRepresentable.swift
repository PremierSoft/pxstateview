//
//  PxStateButtonRepresentable.swift
//  PxStateView
//
//  Created by Orlando Amorim on 12/06/19.
//  Copyright © 2019 Pixelwolf. All rights reserved.
//

import Foundation

public protocol PxStateButtonRepresentable: UIButton {
    var onTap: ((_ button: PxStateButtonRepresentable) -> Void)? { get set }
}
