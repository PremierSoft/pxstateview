//
//  PxStateElementView.swift
//  PxStateView
//
//  Created by Orlando Amorim on 13/06/19.
//  Copyright © 2019 Pixelwolf. All rights reserved.
//

import UIKit

/**
 * A view that wraps every element in a stack view.
 */

class PxStateElementView: UIView {
    
    // MARK: Lifecycle
    init(contentView: UIView, with contraints: PxStateViewElementConstraints) {
        self.contentView = contentView
        self.elementConstraints = contraints
        super.init(frame: .zero)
        translatesAutoresizingMaskIntoConstraints = false
        if #available(iOS 11.0, *) {
            insetsLayoutMarginsFromSafeArea = false
        }
        
        setupViews()
        setupConstraints()
    }
    
    @available(*, unavailable)
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    // MARK: Public
    let contentView: UIView
    var elementConstraints: PxStateViewElementConstraints
    
    var elementInset: UIEdgeInsets {
        get { return layoutMargins }
        set { layoutMargins = newValue }
    }
    
    func update(contraints: PxStateViewElementConstraints) {
        self.elementConstraints = contraints
        elementInset = contraints.getInset()
    }
    
    // MARK: Private
    private func setupViews() {
        setupSelf()
        setupContentView()
    }
    
    private func setupSelf() {
        clipsToBounds = true
        backgroundColor = .clear
        isUserInteractionEnabled = true
    }
    
    private func setupContentView() {
        contentView.translatesAutoresizingMaskIntoConstraints = false
        contentView.isUserInteractionEnabled = true
        addSubview(contentView)
    }
    

    private func setupConstraints() {
        setupContentViewConstraints()
        elementInset = elementConstraints.getInset()
    }
    
    private func setupContentViewConstraints() {
        let bottomConstraint = contentView.bottomAnchor.constraint(equalTo: layoutMarginsGuide.bottomAnchor)
        bottomConstraint.priority = UILayoutPriority(rawValue: UILayoutPriority.required.rawValue - 1)
        
        NSLayoutConstraint.activate([
            contentView.leadingAnchor.constraint(equalTo: layoutMarginsGuide.leadingAnchor),
            contentView.trailingAnchor.constraint(equalTo: layoutMarginsGuide.trailingAnchor),
            contentView.topAnchor.constraint(equalTo: layoutMarginsGuide.topAnchor),
            bottomConstraint
            ])
    }
}
