//
//  PxStateView.swift
//  PxStateView
//
//  Created by Orlando Amorim on 12/06/19.
//  Copyright © 2019 Pixelwolf. All rights reserved.
//

import UIKit

public class PxStateView: UIScrollView {
    
    // MARK: - Vars
    private var representable: PxStateRepresentable?
    
    // MARK: - Views
    private let stackView: UIStackView = {
        let stackView = UIStackView()
        stackView.backgroundColor = UIColor.white
        stackView.translatesAutoresizingMaskIntoConstraints = false
        stackView.axis = .vertical
        return stackView
    }()

    // MARK: - Setup
    public init(with representable: PxStateRepresentable? = nil) {
        super.init(frame: .zero)
        self.representable = representable
        setupViews()
        setupElements()
    }
    
    @available(*, unavailable)
    required public init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    // MARK: - Private
    private func setupViews() {
        setupSelf()
        setupStackView()
    }
    
    private func setupSelf() {
        backgroundColor = UIColor.white
        translatesAutoresizingMaskIntoConstraints = false
        showsVerticalScrollIndicator = false
        showsHorizontalScrollIndicator = false
    }
    
    private func setupStackView() {
        addSubview(stackView)
        NSLayoutConstraint.activate([
            stackView.topAnchor.constraint(equalTo: topAnchor),
            stackView.bottomAnchor.constraint(equalTo: bottomAnchor),
            stackView.leadingAnchor.constraint(equalTo: leadingAnchor),
            stackView.trailingAnchor.constraint(equalTo: trailingAnchor),
            stackView.widthAnchor.constraint(equalTo: widthAnchor)
            ])
    }
    
    public func getRepresentable() -> PxStateRepresentable? {
        return representable
    }
    
    public func update(representable: PxStateRepresentable?) {
        self.representable = representable
        setupElements()
    }

    private func setupElements() {
        stackView.removeAllArrangedSubviews()
        guard let representable = representable else {
            return
        }
        representable.elements.forEach { element in
            switch element.type {
            case .view(let view):
                setupElementView(with: view, and: element.constraints)
            case .image(let image):
                let view = makeImageView(with: image)
                setupElementView(with: view, and: element.constraints)
            case .text(let text, let style):
                let attributedText = NSAttributedString(string: text, attributes: style)
                let view = makeLabel(with: attributedText)
                setupElementView(with: view, and: element.constraints)
            case .attributedText(let attributedText):
                let view = makeLabel(with: attributedText)
                setupElementView(with: view, and: element.constraints)
            case .button(let button):
                let view = makeButton(with: button)
                setupElementView(with: view, and: element.constraints)
            }
        }
    }
    
    private func setupElementView(with view: UIView, and constraints: PxStateViewElementConstraints) {
        if let height = constraints.height {
            view.heightAnchor.constraint(equalToConstant: height).isActive = true
        } else if let aspectRatio = constraints.aspectRatio {
            view.heightAnchor.constraint(equalTo: view.widthAnchor, multiplier: aspectRatio).isActive = true
        }
        
        let elementView = PxStateElementView(contentView: view, with: constraints)
        stackView.addArrangedSubview(elementView)
    }
    
    // MARK: - Button Action
    @objc func onActionButtonTap(sender: UIButton) {
        guard let button = sender as? PxStateButtonRepresentable else { return }
        button.onTap?(button)
    }
}

// MARK: - Factory Methods
extension PxStateView {
    private func makeImageView(with image: UIImage) -> UIImageView {
        let imageView = UIImageView(frame: .zero)
        imageView.translatesAutoresizingMaskIntoConstraints = false
        imageView.image = image
        imageView.contentMode = .scaleAspectFit
        imageView.clipsToBounds = true
        imageView.backgroundColor = .clear
        return imageView
    }
    
    private func makeLabel(with attributedText: NSAttributedString) -> UILabel {
        let label = UILabel(frame: .zero)
        label.translatesAutoresizingMaskIntoConstraints = false
        label.numberOfLines = 0
        label.textAlignment = .center
        label.attributedText = attributedText
        label.backgroundColor = .clear
        return label
    }
    
    private func makeButton(with button: PxStateButtonRepresentable) -> PxStateButtonRepresentable {
        button.translatesAutoresizingMaskIntoConstraints = false
        button.addTarget(self, action: #selector(onActionButtonTap), for: .touchUpInside)
        return button
    }
}
