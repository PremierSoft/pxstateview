//
//  PxStateViewConfiguration.swift
//  PxStateView
//
//  Created by Orlando Amorim on 12/06/19.
//  Copyright © 2019 Pixelwolf. All rights reserved.
//

import Foundation

public struct PxStateViewConfiguration {
    var constraints: PxStateViewElementConstraints
    var elementsSpacing: CGFloat
    
    public init(constraints: PxStateViewElementConstraints = PxStateViewElementConstraints(), elementsSpacing: CGFloat = .zero) {
        self.constraints = constraints
        self.elementsSpacing = elementsSpacing
    }
}
