//
//  PxStateViewElement.swift
//  PxStateView
//
//  Created by Orlando Amorim on 12/06/19.
//  Copyright © 2019 Pixelwolf. All rights reserved.
//

import Foundation

public struct PxStateViewElement {
    enum ElementType {
        case view(UIView)
        case image(UIImage)
        case text(text: String, style: [NSAttributedString.Key: Any]?)
        case attributedText(attributedText: NSAttributedString)
        case button(PxStateButtonRepresentable)
    }
    
    public var constraints: PxStateViewElementConstraints
    private(set) var type: ElementType
    
    public init(view: UIView, constraints: PxStateViewElementConstraints = PxStateViewElementConstraints()) {
        self.type = .view(view)
        self.constraints = constraints
    }
    
    public init(image: UIImage, constraints: PxStateViewElementConstraints = PxStateViewElementConstraints()) {
        self.type = .image(image)
        self.constraints = constraints
    }
    
    public init(text: String, style: [NSAttributedString.Key: Any]? = nil, constraints: PxStateViewElementConstraints = PxStateViewElementConstraints()) {
        self.type = .text(text: text, style: style)
        self.constraints = constraints
    }
    
    public init(attributedText: NSAttributedString, constraints: PxStateViewElementConstraints = PxStateViewElementConstraints()) {
        self.type = .attributedText(attributedText: attributedText)
        self.constraints = constraints
    }
    
    public init(button: PxStateButtonRepresentable, constraints: PxStateViewElementConstraints = PxStateViewElementConstraints()) {
        self.type = .button(button)
        self.constraints = constraints
    }
}
