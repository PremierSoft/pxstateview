//
//  PxStateViewElementConstraints.swift
//  PxStateView
//
//  Created by Orlando Amorim on 12/06/19.
//  Copyright © 2019 Pixelwolf. All rights reserved.
//

import Foundation

public struct PxStateViewElementConstraints {
    public var top: CGFloat
    public var leading: CGFloat
    public var trailing: CGFloat
    public var bottom: CGFloat
    public var height: CGFloat?
    public var aspectRatio: CGFloat?

    public init(top: CGFloat = 0.0, leading: CGFloat = 0.0, trailing: CGFloat = 0.0, bottom: CGFloat = 0.0, height: CGFloat? = nil, aspectRatio: CGFloat? = nil) {
        self.top = top
        self.leading = leading
        self.trailing = trailing
        self.bottom = bottom
        self.height = height
        self.aspectRatio = aspectRatio
    }
    
    func getInset() -> UIEdgeInsets {
        return UIEdgeInsets(top: top, left: leading, bottom: bottom, right: trailing)
    }
}
