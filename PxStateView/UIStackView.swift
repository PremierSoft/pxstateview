//
//  UIStackView.swift
//  PxStateView
//
//  Created by Orlando Amorim on 13/06/19.
//  Copyright © 2019 Pixelwolf. All rights reserved.
//

import UIKit.UIStackView

public extension UIStackView {
    func removeAllArrangedSubviews() {
        arrangedSubviews.forEach({
            removeArrangedSubview($0)
            $0.removeFromSuperview()
        })
    }
}

